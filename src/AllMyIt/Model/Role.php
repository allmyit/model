<?php namespace AllMyIt\Model;

class Role extends BaseModel
{

	protected $guarded = [];

	public static $rules = [
		'name' => 'required'
	];

	public function users()
	{
		return $this->hasMany('User');
	}

	public function __toString()
	{
		return $this->name;
	}

}
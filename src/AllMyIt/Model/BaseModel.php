<?php namespace AllMyIt\Model;

use Closure;
use LaravelBook\Ardent\Ardent;

class BaseModel extends Ardent {

	private function getCallingNamespace()
	{
		//Get the calling class
		$currentClass = get_class($this);
		//Call the class to get the 
		$refl = new \ReflectionClass($currentClass);
		$namespace = $refl->getNamespaceName();

		return $namespace."\\";
	}

	public function hasOne($related, $foreignKey = null, $localKey = NULL)
	{
		$namespace = '';

		if (substr($related, 0, 1) != '\\') {
			$namespace = $this->getCallingNamespace();
		}
		
		return parent::hasOne($namespace.$related, $foreignKey, $localKey);
	}

	public function  morphOne($related, $name, $type = null, $id = null, $localKey = NULL)
	{
		$namespace = $this->getCallingNamespace();
		return parent::morphOne($namespace.$related, $name, $type, $id, $localKey);
	}

	public function belongsTo($related, $foreignKey = null, $otherKey = NULL, $relation = NULL)
	{
		$namespace = '';

		if (substr($related, 0, 1) != '\\') {
			$namespace = $this->getCallingNamespace();
		}

		/**
		 * The foreign key is automaticly generated as the called function
		 * name _id
		 */
		if (is_null($foreignKey)) {
			list(, $caller) = debug_backtrace(false);
			$foreignKey = snake_case($caller['function']) . '_id';
		}

		return parent::belongsTo($namespace.$related, $foreignKey, $otherKey, $relation);
	}

	public function hasMany($related, $foreignKey = null, $localKey = NULL) 
	{
		/**
		 * We dont need to check the foreign key as its based on the
		 * class name of the calling function.
		 */
		$namespace = $this->getCallingNamespace();
		return parent::hasMany($namespace.$related, $foreignKey, $localKey);
	}

	public function belongsToMany($related, $table = null, $foreignKey = null, $otherKey = null, $relation = NULL)
	{
		if (is_null($foreignKey)) {
			list(, $caller) = debug_backtrace(false);
			$foreignKey = snake_case($caller['function']) . '_id';
		}

		$namespace = $this->getCallingNamespace();
		return parent::belongsToMany($namespace.$related, $table,  $foreignKey, $otherKey, $relation);
	}

	/*
	 * Get validation rules
	 */
	
	public function getRules()
	{
		return self::$rules;
	}

	/*
	 * Scope to get all records that are active
	 */

	public function scopeActive($query)
	{
		return $query->where('active', '=', true);
	}

	/**
	 * Get all active items and return a grouped array
	 * Used for add select boxes
	 */
	public static function activeAdd($columns = array('name', 'id')) {
		$return = [];

		foreach(parent::active()->get($columns) as $item)
        {
            $return[$item->id] = $item->__toString();
        }
        $return['---']['add'] = "Add";

        return $return;
    }
}